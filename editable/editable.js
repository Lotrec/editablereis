
let data ; 
let tableElem = document.createElement('table');

function createTable(elem){

    let jsonURL = elem.getAttribute('data-source') 
    
    fetch(jsonURL)
    .then((response)=> response.json())
    .then((lesDatasDuFichier) => {
        data = lesDatasDuFichier 
        tableHeader()
        tableBody()
        });

    elem.appendChild(tableElem)
}
function tableHeader(){
    let unElement = data[0]
    let tr = document.createElement('tr')
    for(let key in unElement){                   // for in parcour les clef
        let th = document.createElement('th')
        th.innerText = key
        th.onclick = function(){
                if(typeof(unElement[key])=== 'string'){
                    data.sort(function (a, b) {
                        return a[key].localeCompare(b[key]);
                    })}
                    else{
                data = data.sort((a, b) => {
                    return a[key] - b[key];
                });
            }
        tableBody()
            }
        tr.appendChild(th)
    }
    let thead= document.createElement('thead')
    thead.classList.add('tbl-header')
    thead.appendChild(tr)
    tableElem.appendChild(thead)
}

function tableBody(){
    let tbody = document.createElement('tbody')

    for(let obj of data){                           // for of parcour les valeurs
        let tr = document.createElement('tr')
        for(let key in obj){
            let td = document.createElement('td')
            td.className = "table-data"
            td.contentEditable = true;
            td.innerText = obj[key]
            td.addEventListener('input', e => {
                obj[key] = td.innerText;
                
            })
            tr.appendChild(td)
        }
        tbody.appendChild(tr)
    }
    tableElem.querySelector('tbody')?.remove()
    tableElem.appendChild(tbody) 
}
function filtreTable(){
    
    let input= document.getElementById('filtrant').value.toLowerCase();
        let tbody = tableElem.getElementsByTagName('tbody')[0];
        let tr = tbody.getElementsByTagName('tr')
        
        for(let i =0; i < tr.length; i++) {

            let dataCells = tr[i].getElementsByTagName('td');

            let match = false;

            for(let j = 0; j< dataCells.length; j++) {
                if (dataCells[j].innerText.toLowerCase().indexOf(input) > -1) {
                    match = true;
                    break;
                }
            }
            if (match) {
                tr[i].style.display = "";
            }
            else {
                tr[i].style.display = "none";
            }}}


saveDiv = document.getElementById('save-button');
saveDatas = document.createElement('button');
saveDatas.innerText ="save";
saveDiv.appendChild(saveDatas);
saveDatas.addEventListener('click', function() {
    let dataJson = JSON.stringify(data)
    fetch('../test/up.php',{
        method: 'POST',
        body: dataJson
    })
})